﻿<%@ Page Title="" Language="C#" MasterPageFile="/Site.Master" AutoEventWireup="true" CodeBehind="VoTech.aspx.cs" Inherits="ProfileWebsite.Projects.VoTech" %>

<asp:Content ID="Content2" ContentPlaceHolderID="contentHolder" runat="server">
    <div class="row">
        <div class="col-md-8">
            <br />
            <h3>Vo-Tech</h3>
            <p><b>Vo for Speech Recognition Voice Technology!</b></p>
            <p><b>Tech for the Latest Technology!</b></p>
            <p><b>This is the Latest Technology for alarm clock systems, incorporating speech recognition!</b></p>

            <p>Vo-Tech is a speech recognition alarm clock made specifically for immobile individuals and individuals who strive for the latest technology!</p>
            <p>Vo-Tech allows you to simply speak into the alarm clock in order to turn off your alarm or put your alarm to sleep!</p>
            <p>Normally the alarm clock user is required to get completely up and reach for the snooze of off button on their alarm clocks.</p>
            <p>But, using Vo-Tech, they can now just Say the Word!</p>
            <p>Vo-Tech listens to the user and determines which command to react to, after the option is determine it completed the appropriate reaction.</p>
        </div>
        <div class="col-md-4">
            <img src="http://res.cloudinary.com/joharihughes/image/upload/r_8/v1461635881/VR.jpg" alt="VR" class="img-responsive imgRight" runat="server" />
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <div class="videoWrapper">
                <iframe width="100%" height="100%" src="https://docs.google.com/viewer?srcid=0B-EbjbH4QV6-c0Z3bWlMNXoyVDg&pid=explorer&efh=false&a=v&chrome=false&embedded=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-md-6">
            <br />
            <p>.<b>Vo-Tech Commands:</b></p>
            <p>- <b>"Alarm Off!"</b> - This command turns the alarm completely off.</p>
            <p>- <b>"Sleep!"</b>
            - This command will place alarm in sleep mode for however long the program is set real-time.
            <p>- Vo-Tech allows the customer to get the maximum amount of sleep and alleviates difficulty for immobile users!</p>

            <p><b>VoTech Complete Demo</b></p>

            <p><a href="https://www.youtube.com/watch?v=RcuLTBTxB7g">https://www.youtube.com/watch?v=RcuLTBTxB7g</a></p>

            <p><b>VoTech Tutorial 1</b></p>

            <p><a href="https://www.youtube.com/watch?v=BkmWDECAoTA">https://www.youtube.com/watch?v=BkmWDECAoTA</a></p>

            <p><b>VoTech Tutorial 2</b></p>

            <p><a href="https://www.youtube.com/watch?v=T2L3QGZkjK0">https://www.youtube.com/watch?v=T2L3QGZkjK0</a></p>

            <p>
                <b>VoTech Tutorial 3</b>
            <p>
            <p><a href="https://www.youtube.com/watch?v=vtm92-PakrE">https://www.youtube.com/watch?v=vtm92-PakrE</a></p>
        </div>
    </div>
</asp:Content>
