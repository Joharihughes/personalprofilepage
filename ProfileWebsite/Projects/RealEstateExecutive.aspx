﻿<%@ Page Title="" Language="C#" MasterPageFile="/Site.Master" AutoEventWireup="true" CodeBehind="RealEstateExecutive.aspx.cs" Inherits="ProfileWebsite.Projects.RealEstateExecutive" %>

<asp:Content ID="Content2" ContentPlaceHolderID="contentHolder" runat="server">
    <div class="row">
        <h3 class="center">Real Estate Executive System : Patented Product</h3>
        <div class="col-md-12">
            <p>Real Estate Executive is a system that enables Brokerage Firms to keep track of all of their Real Estate Agents and all of their customers and real estate properties...</p>
            <p><b>- Engineered in VB .Net and recently re-engineered in C#.</b></p>


            <p><b>Real Estate Executive takes of vital information for a Brokerage firm and manages it, so they don't have to!</b></p>
            <p>- No more forgetting agent or customers birthdays the application will notify you!</p>
            <p>- No more difficulty keeping all of your files and information together! Real Estate Executive keeps everything together for you in one secure location.</p>
            <p>- Surf the web for more Real Estate properties, calculate commission splits and send emails all from the application!</p>
            <p>- Save all your lengthy Agreements in one place!</p>
            <p>- Stores Customer and Property first-hand photographs!</p>
            <p>
                <img src="/Images/RealEstateExecutive.jpg" alt="Real Estate Executive" runat="server" class="imgLeft img-responsive" />
                <img src="http://res.cloudinary.com/joharihughes/image/upload/r_15/v1461789446/custPic.jpg" alt="Blank Portrait" class="img-responsive imgRight" runat="server" />
            </p>
            <p><b>Real Estate Executive will store all of your brokerage firms vital information such as:</b></p>
            <p>- Real Estate Agent information!</p>
            <p>- Real Estate Client information!</p>
            <p>- Real Estate Property information!</p>
            <p>- Broker/Agent to Customer Agreements!</p>
            <p>- Broker to Agent Agreements!</p>
            <p>- Stores Customer and Property photographs and reports!</p>
        </div>
    </div>
</asp:Content>
