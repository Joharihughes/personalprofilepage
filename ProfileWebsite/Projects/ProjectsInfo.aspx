﻿<%@ Page Title="" Language="C#" MasterPageFile="/Site.Master" AutoEventWireup="true" CodeBehind="ProjectsInfo.aspx.cs" Inherits="ProfileWebsite.Projects.WebForm1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="contentHolder" runat="server">
    <div class="row">
        <div class="col-md-8">
            <h3>My Personal Projects</h3>
            <p><b>Welcome to my personal profile of projects!</b></p>
            <p>Welcome to my personal profile webpage. This webpage gives a great run through on everything that makes me a great candidate for you, detailing extensive projects and products I've personally innovated!</p>
            <p>This webpage was created to show the usage of the basic ASP .NET.</p>
            <p>Showing the content image formatting this page serves as a modern live resume!</p>
            <h1>Enjoy!</h1>

            <p>These display the ample projects I have taken on within my spare time.</p>
            <p>- One of these items has had a temporary patent.</p>
            <p><b>Throughout these projects I have gained an understanding for innovative technologies, such as:</b></p>
            <p>- A speech recognition system</p>
            <p>- A proximity scanning technology</p>
            <p><b>During this journey, I have worked with codes, such as:</b></p>
            <p>- C, C++, C#, ASP .NET, CSS, jQuery, JavaScript, VB .Net, HTML5, SQL, Arduino</p>

            <h3>Projects</h3>
            <p><a href="/Projects/RealEstateExecutive.aspx">Real Estate Executive System</a></p>
            <p><a href="/Projects/VisioTech.aspx">VisioTech Vision System</a></p>
            <p><a href="/Projects/VoTech.aspx">VoTech Alarm Clock</a></p>
            
        </div>
        <div class="col-md-4">
            <br /><br /><br />
            <img src="http://res.cloudinary.com/joharihughes/image/upload/r_8/v1461635877/projects.jpg" alt="Projects Image" class="img-responsive imgRight" runat="server" style="margin-bottom: 10px" />
        </div>
    </div>

</asp:Content>

