﻿<%@ Page Title="Johari Hughes - Experience" Language="C#" MasterPageFile="/Site.Master" AutoEventWireup="true" CodeBehind="Experience.aspx.cs" Inherits="ProfileWebsite.About" %>

<asp:Content ID="Content2" ContentPlaceHolderID="contentHolder" runat="server">
    <div class="row" style="margin-top: 5px">
        <div class="col-md-2">
        </div>
        <div class="col-md-8" id="center">

            <img src="/Images/ibc.jpg" alt="International Bank of Commerce" runat="server" class="imgRight img-responsive" />

            <div class="center">
                <p><b>WORK HISTORY</b></p>
                <p><b>International Bank of Commerce </b><b>June 2012 - June 2015</b></p>

                <p><b>Software Developer </b>- 2416 Cee Gee Street, San Antonio, TX, 78219 <b>(September 2014 - June 2015)</b></p>

                <p><b>Supervising Coder: Omar Perez - Lead Software Developer (Horizon) - Contact: </b>956.324.4261<b> (M)</b></p>

                <p><b>.Net and C# - </b>Rapidly and efficiently engineered and redesigned primary banking programs and coding</p>

                <p><b>SQL</b> - Efficiently manipulated and accelerated databases and database queries</p>


                <p><b>SQL, Excel</b> - Pioneered and refined intricate user friendly reporting services to reference database members </p>

                <p><b>.Net, C#, Excel, Outlook, SQL</b> - Professionally designed and formulated testing algorithms and documentation</p>

                <p><b>Promotion & Relocation - </b>Surpassed expectations and negotiated eligibility for re-location and promotion</p>

                <p><b>Communication</b> - Effectively communicated with departments relative to personal programming and modification</p>

                <p><b>Communicate Code</b> - Effectively defined and communicated main usages of implementations and coding</p>

                <p><b>Fast-Paced Environment </b>- Effectively reached task and assignment deadlines</p>

                <p><b>Sales Associate - </b>2817 East Cesar Chavez St., Austin, TX, 78702 <b>(June 2014 - September 2014)</b></p>

                <p><b>Supervisor: Mary Hammerle - Bank Officer / Austin General Sales Manager - Contact: </b>210.383.8407<b>Ext: </b>24001<b>(W)</b></p>

                <p>Successfully re-established a base in International Bank of Commerce after graduation</p>

                <p>Successfully conducted business and association with multiple customers through rapport building technique </p>

                <p>Professionally structured and secured customer valuable information with top-security upkeep</p>

                <p>Successfully surpassed complex monthly sales goals, with multiple alternate requirements</p>

                <p>Consistently and professionally introduced customers to all vital and relevant information</p>

                <p><b>Cross Trained = Teller/Sales Associate - </b>2817 East Cesar Chavez St., Austin, TX, 78702 <b>(June 2012 - January 2013)</b></p>

                <p><b>Supervisor: Josue Limon - Branch Manager - Contact: </b>512.320.9557<b> (M)</b></p>

                <p>Implemented extensive cash management and mathematical skills</p>

                <p>Managed physical sector and successfully retained top-security upkeep of personal information</p>

                <p>Both, developed and informed customers professionally informed of the most vital and relevant information</p>

                <p>Delegated personal checks and successfully kept items at top-security upkeep</p>

                <p>Successfully surpassed monthly sales goals with multiple alternate requirements, utilizing the cross-sale strategy</p>
            </div>
            <img src="/Images/uf.jpg" alt="University Federal Credit Union" runat="server" class="imgLeft img-responsive" />
            <div class="center">
                <p><b>University Federal Credit Union</b> - 130 E. Ben White, Austin, TX, 78704 <b>May 2013 - October 2013</b></p>

                <b>
                    <p>Cross Trained = Teller/Financial Services Representative</p>

                </b>
                <p>Supervisor: Rachel Carter - Branch Manager -- <b>Contact:</b> 512.820.6973 <b>(M)</b></p>

                <p>Formulated advanced teller sales routines, while maintaining daily sales goals </p>

                <p>Successfully exceeded expectations and provided top-experience customer service</p>

                <p>Consistently secured and surpassed monthly sales quotas </p>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</asp:Content>

