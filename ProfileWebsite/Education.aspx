﻿<%@ Page Title="Johari Hughes - Education" Language="C#" MasterPageFile="/Site.Master" AutoEventWireup="true" CodeBehind="Education.aspx.cs" Inherits="ProfileWebsite.Contact" %>


<asp:Content ID="Content2" ContentPlaceHolderID="contentHolder" runat="server">

    <div class="row" style="margin-top: 5px">
        <div class="col-md-2">
        </div>
        <div class="col-md-8" id="center">

            <p><b>Johari Hughes</b></p>

            <p>Brooklyn Park, Minnesota, 55443 - 512.529.4574 (M) - <a href="mailto:JoHughes114@gmail.com">JoHughes114@gmail.com</a></p>

            <h3><b>EDUCATION</b></h3>
            <p>
                <img src="/Images/champion2.jpg" alt="Champions School of Real Estate" class="imgRight img-responsive" />
            </p>

            <p><b>Champions School of Real Estate, </b>San Antonio, Texas <b>Graduate April 2015</b></p>

            <p><b>Certificates:</b></p>

            <p>Real Estate Principles</p>

            <p>Real Estate Principles II</p>

            <p>Real Estate Finance</p>

            <p>Law of Agency</p>

            <p>Law of Contracts</p>

            <p>Promulgated Contract Forms</p>



            <p><b>St. Edward's University,</b> Austin, Texas </p>
            <p><b>Graduate May 2014</b></p>

            <b>
                <p>Bachelor of Science in Computer Information Science</p>
            </b>
            <img src="/Images/stedwards.jpg" alt="St. Edwards University" runat="server" class="imgLeft img-responsive" />
            <p>
                <b>Relevant Courses: </b>
                <p>Senior Seminar (Final Project),</p>
            <p>Software Engineering I &amp; II, </p>
            <p>Web Programming, </p>
            <p>Computer Concepts, </p>
            <p>Database &amp; Database Theory,</p>
            <p>Business Management, </p>
            <p>Business Statistics, </p>
            <p>Presentational Speaking</p>

            <b>
                <p>CERTIFICATIONS</p>

            </b>
            <p>BSA Certification</p>

            <p>OFAC Certification</p>

            <p>GLBA Certification</p>

            <p>Identity Theft Certification</p>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</asp:Content>



