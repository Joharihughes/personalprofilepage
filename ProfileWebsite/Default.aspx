﻿<%@ Page Title="Johari Hughes - Profile" Language="C#" MasterPageFile="/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ProfileWebsite._Default" %>

<asp:Content ContentPlaceHolderID="contentHolder" runat="server">
    <div class="row" style="margin-top: 5px">

        <div class="col-md-12">
            <p>
                <b><i>Highly motivated, results-oriented, Software Developer and Programmer with a Bachelor’s degree in Computer Information Science and primary knowledge in C#, .Net, SQL, and MVC. Currently in search of a full time position in a company that can offer a greater challenge and an environment with an opportunity for growth and advancement.
                </i></b>
            </p>
        </div>
    </div>
    <div class="row">

        <div class="col-md-7">
            <p><b>TECHNICAL SKILLS</b></p>
            <p><b>Operating Systems:</b> Windows; Linux</p>
            <p><b>Languages:</b> .Net, C#, MVC, Bootstrap, JavaScript, CSS, HTML5, Razor, PHP VB .Net, XML, XHTML</p>
            <p><b>Database Manipulation:</b> SQL, Entity Framework, Microsoft Azure, LocalDB, phpMyAdmin; Bitbucket, GitHub</p>
            <p><b>Queries:</b> SQL Reporting Services, SQL Server Management Studios, LINQ, ADO .Net</p>
            <p><b>Applications:</b> Visual Studios, SQL Server, Visual Studios, Eclipse, Terminal/Command Line, Microsoft Office (Word, Excel, PowerPoint, Outlook and Access)</p>
            <p><b>Maintenance Capabilities:</b> Program Engineering and Development, Program Re-Structuring, Database Manipulation, Physical Product Engineering</p>
            <p><b>PAST CERTIFICATIONS</b></p>
            <p>BSA Certification</p>
            <p>OFAC Certification</p>
            <p>GLBA Certification</p>
            <p>Identity Theft Certification</p>
        </div>

        <div class="col-md-4">
            <img src="http://res.cloudinary.com/joharihughes/image/upload/r_8/v1461635876/profilePic.jpg" alt="Portrait" class="img-responsive" style="width: auto;" runat="server" />
        </div>
        <div class="col-md-1">
        </div>
    </div>
</asp:Content>



