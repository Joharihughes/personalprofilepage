﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ProfileWebsite.Startup))]
namespace ProfileWebsite
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
