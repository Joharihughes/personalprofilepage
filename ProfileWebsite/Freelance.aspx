﻿<%@ Page Title="Johari Hughes - Freelance" Language="C#" MasterPageFile="/Site.Master" AutoEventWireup="true" CodeBehind="Freelance.aspx.cs" Inherits="ProfileWebsite.Projects.WebPageInfo" %>

<asp:Content ID="Content2" ContentPlaceHolderID="contentHolder" runat="server">
    <div class="row">
        <center><h2>
        Freelance Experience
    </h2>
    
            <p><b>All of these Freelance experiences, include:</b></p>
            <p>- Working extremely close with a client/business</p>
            <p>- Several development iterations</p>
            <p>- Majority of these contents: <br /><br /></p></center>
    </div>

    <div class="row">

        <div class="col-md-6">
            <p><b>FREELANCE PROJECTS, INCLUDE:</b></p>
            <p><b>ASP .NET</b> – Successfully utilized current ASP .NET to develop consistent web page templates, <b>includes:</b></p>
            <p><b>- Master Page</b> – creates a consistent look through all web pages in personal site and constant navigation</p>
            <p><b>- Bootstrap</b> – Successfully created an organized graphical format and constant responsiveness</p>
            <p><b>- JavaScript</b> – Successfully reformatted the look and feel of page for user responsiveness, such as:</p>
            <p><b>Navigation Division Bar, Banner Realignment, 3D Pop Shadow, and Animations.</b></p>
            <p><b>- HTML</b> – Successfully utilized HTML around asp: tags </p>
            <p><b>- CSS</b> - Successfully coded intricate website design with embedded images/videos, style sheets</p>
            <p><b>- BitBucket/Git</b> – Successfully pushed contents of several projects and various codes to developmental host backup</p>
            <p><b>- Design</b> – Successfully created a warm and welcoming professional display with consistent look and feel</p>
            <p>
                <b>- MVC</b> – Successfully transferred over C# data to MVC platform for mobile usage 
                <br />
                <b>(Real Estate Executive Only)</b>
            </p>

        </div>
        <div class="col-md-6">
            <img src="/Images/iteration1_DG.jpg" alt="HealthInHand Iterations" runat="server" class="img-responsive imgRight border_slide" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div id="myCarousel2" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel2" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel2" data-slide-to="1"></li>
                    <li data-target="#myCarousel2" data-slide-to="2"></li>
                    <li data-target="#myCarousel2" data-slide-to="3"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox" style="margin-top: 20px; padding-top: 20px;">

                    <div class="item active">
                        <center><img src="/Images/Iteration1.jpg" class="img-responsive border_slide" alt="iteration1"/></center>
                    </div>

                    <div class="item">
                        <center><img src="/Images/Iteration2.jpg" class="img-responsive border_slide" alt="iteration2"" /></center>
                    </div>
                    <div class="item">
                        <center><img src="/Images/Iteration3.jpg" class="img-responsive border_slide" alt="iteration3"" /></center>
                    </div>
                    <div class="item">
                        <center><img src="/Images/Iteration4.jpg" class="img-responsive border_slide" alt="iteration4"" /></center>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-6" style="text-align: left">
            <p style="text-align: left"><b>FREELANCE EXPERIENCE</b></p>
            <p>
                <b>Friendly Hours Pediatric Urgent Care</b>
                <br />
                Dr. Tuere Kapenzi – Pediatrician – Ph:1(336)354-9715 - <b>January - Current 2016</b>
                <br />
                Operational Demo:
                <asp:HyperLink ID="hyp" NavigateUrl="http://www.JohariHughes.com/FriendlyHoursPediatrics/Default.aspx" runat="server" Text="click" Target="_blank">http://www.JohariHughes.com/FriendlyHoursPediatrics</asp:HyperLink>
            </p>
            <p>
                <b>Health In Hand</b>
                <br />
                Dr, Bernadette Hightower – OBGYN – Ph: 1(320)492-3730 - <b>February - Current 2016</b>
                <br />
                Operational Demo:
                <asp:HyperLink ID="HyperLink1" NavigateUrl="http://www.JohariHughes.com/HealthInHand" runat="server" Text="click" Target="_blank">http://www.JohariHughes.com/HealthInHand</asp:HyperLink>
            </p>
            <p>
                <b>Real Estate Executive</b>
                <br />
                Temporary Patent - Personal Real Estate Program - <b>June 2015 - Current 2016</b>
                <br />
                Project Marketing Page:
                <asp:HyperLink ID="HyperLink2" NavigateUrl="/RealEstateExecutive.aspx" runat="server" Text="click" Target="_blank">http://www.JohariHughes.com/RealEstateExecutive</asp:HyperLink>
            </p>
            <p>
                <b>Motion Design Portfolio</b>
                <br />
                Jawara Hughes – Motion Designer – Ph:(763)232-6603 - <b>March - May 2016</b>
                <br />
                Operational Demo:
                <asp:HyperLink ID="HyperLink3" NavigateUrl="http://www.JohariHughes.com/Portfolio-Jawara" runat="server" Text="click" Target="_blank">http://www.JohariHughes.com/Portfolio-Jawara</asp:HyperLink>

            <p>
                <b>Photographer Gallery</b>
                <br />
                Ammanuel Brown - Photographer - <b>January - February 2016</b>
                <br />
                Operational Demo:
                <asp:HyperLink ID="HyperLink5" NavigateUrl="http://www.JohariHughes.com/DesignerGallery" runat="server" Text="click" Target="_blank">http://www.JohariHughes.com/DesignerGallery</asp:HyperLink>
            </p>
     <p>
                <b>One Page Interactive Personal Profile</b>
                <br />
                One Page Interactive Profile - <b>Completed May 2016</b>
                <br />
                One Page Profile:
                <asp:HyperLink ID="HyperLink4" NavigateUrl="http://www.JohariHughes.com/Profile-OnePage" runat="server" Text="click" Target="_blank">http://www.JohariHughes.com/Profile-OnePage</asp:HyperLink>
            </p>
        </div>
    </div>

</asp:Content>





