﻿<%@ Page Title="" Language="C#" MasterPageFile="/Site.Master" AutoEventWireup="true" CodeBehind="VisioTech.aspx.cs" Inherits="ProfileWebsite.Projects.VisioTech" %>

<asp:Content ID="Content2" ContentPlaceHolderID="contentHolder" runat="server">

    <div class="row">
      <div class="col-md-8">
            <h3>Visio-Tech</h3>
            <p><b>Visio for Visual Assistance!</b></p>
            <p><b>Tech for the Latest Technology!</b></p>
            <p><b>This is the Latest Technology for vision the visually impaired!</b></p>
            <p>Visio-Tech is a proximity scanning technology made specifically for the visually impaired!</p>
            <p>Visio-Tech tells its user if something is directly in front of them and how far they are away!</p>
            <p>Normally the blind are forced to use clumsy canes in order to feel where they are going, or are required to purchase and raise their own seeing eye dog! ut, now visually impaired citizens have another option.</p>
            <p>They can now use the Visio-Tech system that will simply tell them exactly how far someone is away from them or if someone walks into their path.</p>
            <p>Visio-Tech measure the distances between the product and the any other object, meaning people or other obstructions.</p>
</div>
<div class="col-md-4">
<img src="/Images/Visio-Tech.jpg" alt="Display Vision" class="img-responsive img-rounded imgRight" runat="server" />
        </div>
</div>
    <br /><br />
    <div class="row">
        <div class="col-md-6">
            <div class="videoWrapper">
                <iframe width="100%" height="100%" src="https://docs.google.com/viewer?srcid=0B-EbjbH4QV6-TjFJUm13NGh1eW8&pid=explorer&efh=false&a=v&chrome=false&embedded=true" runat="server" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-md-6">
            <p><b>Benefits:</b></p>
            <p>- Visio-Tech is the most up-to-date technology out!</p>
            <p>- Visio-Tech only requires one operator!</p>
            <p>- Visio-Tech takes away the need for a walking stick!</p>
        </div>
    </div>

</asp:Content>










